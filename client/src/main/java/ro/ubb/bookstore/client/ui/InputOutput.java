package ro.ubb.bookstore.client.ui;
import ro.ubb.bookstore.common.MasterService;
import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.Pair;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Class for reading user input and printing to console.
 *
 * @author Adi, Tudor
 *
 */
public class InputOutput {
    private Scanner scanner = new Scanner(System.in);

    private MasterService service;
    public InputOutput(MasterService services){service=services;}

    /**
     * Read user input and add Book to the repository.
     * @return The book that has been read from the keyboard.
     *
     */
    private Book readBook(){
        System.out.println("ID:");
        Long id = scanner.nextLong();
        scanner.nextLine(); // Must be here otherwise Title is not read, reason: nextLong doesn't read new line?

        System.out.println("Title:");
        String title = scanner.nextLine();

        System.out.println("Author:");
        String author = scanner.nextLine();

        System.out.println("Price:");
        Long price = scanner.nextLong();
        scanner.nextLine();

        System.out.println("Release Year:");
        int year = scanner.nextInt();

        Book book = new Book(title,author,price,year);
        book.setId(id);
        return book;
    }
    private void addBook(){
        Book book=readBook();
        // Attempt to add book, if validator doesn't pass then print exception
        try {
            if(!service.addBook(book).equals(Book.INVALID)){
                System.out.println(book);
                System.out.println("Id is already in use");
            }else{
                System.out.println("Book successfully added");
            };
        }
        catch (ValidatorException e)
        {
            System.out.println(e.toString());
        }

    }

    /**
     * Show all the books in the repository.
     *
     */
    private void showBooks(){
        service.getAllBooks().forEach(System.out::println);
    }
    private void bookYear(){
        System.out.println("Find books released in:");
        int x = scanner.nextInt();
        Set<Book> res=service.findByYear(x);
        if(res.isEmpty())
            System.out.println("Dry year.No results :/");
        else
            res.stream().forEach(System.out::println);
    }
    private void bookName(){
        System.out.println("Find books with name like:");
        scanner.nextLine();
        String x = scanner.nextLine();
        Set<Book> res=service.findByName(x);
        if(res.isEmpty())
            System.out.println("Couldn't find sequence in any title");
        else
            res.stream().forEach(System.out::println);
    }
    private void removeBook(){
        System.out.println("remove book with ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        try{
            Book ob=service.removeBook(id);
            if(!ob.equals(Book.INVALID)){
                System.out.println(ob.getTitle()+" was removed");
            }else{
                System.out.println("Could not find any book with that ID.No changes made");
            }
        }catch (IllegalArgumentException e){
            System.out.println(e.toString());
        }
    }
    private void updateBook(){
        System.out.println("Specify current book ID and new information:");
        Book book=readBook();
        try{
            Book ob=service.update(book);
            if(!ob.equals(Book.INVALID)){
                System.out.println("Update successful");
            }else{
                System.out.println("Could not find update target.No changes made");
            }
        }catch (ValidatorException e){
            System.out.println(e.toString());
            System.out.println("Update failed.No changes made");
        }
    }
    private void bookMenu(){
        while(true){
            System.out.println("1.Add book|2.Show all books|3.Remove book|4.Update book|5.Find by year|6.Find by author|7.Sort Title|0.Go back");
            int x = scanner.nextInt();
            switch(x){
                case 1:
                    addBook();
                    break;
                case 2:
                    showBooks();
                    break;
                case 3:
                    removeBook();
                    break;
                case 4:
                    updateBook();
                    break;
                case 5:
                    bookYear();
                    break;
                case 6:
                    bookName();
                    break;
                case 7:
                    try{
                        service.sortTitle().forEach(System.out::println);
                    }catch(ValidatorException e){e.printStackTrace();}
                case 0:
                    return;
                default:
                    System.out.println("Invalid input");;
            }

        }
    }
    private Client readClient(){
        System.out.println("ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        System.out.println("First Name:");
        String first = scanner.nextLine();

        System.out.println("Last Name:");
        String last = scanner.nextLine();

        System.out.println("Address:");
        String addr = scanner.nextLine();
        Client client = new Client(first,last,addr);
        client.setId(id);
        return client;
    }
    private void addClient(){
        Client client=readClient();
        // Attempt to add book, if validator doesn't pass then print exception
        try {
            if(!service.addClient(client).equals(Client.INVALID)){
                System.out.println("Id is already in use");
            }else{
                System.out.println("Client successfully added");
            };
        }
        catch (ValidatorException e)
        {
            System.out.println(e.toString());
        }
    }
    private void showClients(){
        service.getAllClients().forEach(System.out::println);
    }
    private void removeClient(){
        System.out.println("remove client with ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        try{
            Client ob=service.removeClient(id);
            if(!ob.equals(Client.INVALID)){
                System.out.println(ob.getName()+" was removed");
            }else{
                System.out.println("Could not find any client with that ID.No changes made");
            }
        }catch (IllegalArgumentException e){
            System.out.println(e.toString());
        }
    }
    private void updateClient(){
        System.out.println("Specify current client ID and new information:");
        Client client=readClient();
        try{
            Client ob=service.update(client);
            if(!ob.equals( Client.INVALID)){
                System.out.println("Update successful");
            }else{
                System.out.println("Could not find update target.No changes made");
            }
        }catch (ValidatorException e){
            System.out.println(e.toString());
            System.out.println("Update failed.No changes made");
        }
    }
    private void clientAddr(){
        System.out.println("Find clients with address like:");
        scanner.nextLine();
        String x = scanner.nextLine();
        Set<Client> res=service.findByAdress(x);
        if(res.isEmpty())
            System.out.println("No client with matching address");
        else
            res.stream().forEach(System.out::println);
    }
    private void clientMenu(){
        while(true){
            System.out.println("1.Add client|2.Show all clients|3.Remove client|4.Update client|5.Find by address|0.Go back");
            int x = scanner.nextInt();
            switch(x){
                case 1:
                    addClient();
                    break;
                case 2:
                    showClients();
                    break;
                case 3:
                    removeClient();
                    break;
                case 4:
                    updateClient();
                    break;
                case 5:
                    clientAddr();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Invalid input");;
            }

        }
    }
    /**
     * Start the menu application.
     */
    private void buyBook(){
        System.out.println("Purchase number:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Client ID:");
        Long cid = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Book ID:");
        Long bid = scanner.nextLong();
        scanner.nextLine();
        Purchase pur=new Purchase(cid,bid);
        pur.setId(id);
        try {
            if(!service.addPurchase(pur).equals(Purchase.INVALID)){
                System.out.println("Limited to 1 copy of the same book per customer");
            }else{
                System.out.println("Book bought GJ");
            };
        }
        catch (ValidatorException e)
        {
            System.out.println(e.toString());
        }

    }
    public void menu(){
        while(true)
        {
            System.out.println("1.Manage Books|2.Manage Clients|3.Buy Book|4.Sale History|5.Most Spent per Client|6.Most bought books|0.exit");
            int x = scanner.nextInt();
            switch(x) {
                case 1:
                    bookMenu();
                    break;
                case 2:
                    clientMenu();
                    break;
                case 3:
                    buyBook();
                    break;
                case 4:
                    service.formatPurchases().stream().forEach(System.out::println);
                    break;
                case 5:
                    service.spentMost().stream().forEach(pair->System.out.println(pair.getKey().getName()+":"+pair.getValue()));
                    break;
                case 6:
                    service.mostBought().stream().forEach(pair->System.out.println(pair.getKey().getTitle()+":"+pair.getValue()));
                    break;
                case 0:
                    scanner.close();
                    return;
                default:
                    System.out.println("Invalid input");
            }
        }
    }
}
