package ro.ubb.bookstore.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import ro.ubb.bookstore.common.MasterService;

@Configuration
public class ClientConfig {
    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean() {
        RmiProxyFactoryBean rmiProxyFactoryBean = new RmiProxyFactoryBean();
        rmiProxyFactoryBean.setServiceInterface(MasterService.class);
        rmiProxyFactoryBean.setServiceUrl("rmi://localhost:1099/MasterService");
        return rmiProxyFactoryBean;
    }
}
