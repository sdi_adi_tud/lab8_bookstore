package ro.ubb.bookstore.client.service;


import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.bookstore.common.MasterService;
import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Set;

import ro.ubb.bookstore.common.Pair;
public class ClientService implements MasterService {
    @Autowired
    private MasterService masterService;

    @Override
    public Client addClient(Client entity) throws ValidatorException {
        return masterService.addClient(entity);
    }

    @Override
    public Set<Client> getAllClients() {
            return masterService.getAllClients();
    }

    @Override
    public Client removeClient(Long id) throws IllegalArgumentException {
        return masterService.removeClient(id);
    }

    @Override
    public Client update(Client entity) throws ValidatorException {
        return masterService.update(entity);
    }

    @Override
    public Set<Client> findByAdress(String addr) {
        return masterService.findByAdress(addr);
    }

    @Override
    public Book addBook(Book entity) throws ValidatorException {
        return masterService.addBook(entity);
    }

    @Override
    public Set<Book> getAllBooks() {
        return masterService.getAllBooks();
    }

    @Override
    public Book removeBook(Long id) throws IllegalArgumentException {
        return masterService.removeBook(id);
    }

    @Override
    public Book update(Book entity) throws ValidatorException {
        return masterService.update(entity);
    }

    @Override
    public Set<Book> findByYear(int year) {
        return masterService.findByYear(year);
    }

    @Override
    public Set<Book> findByName(String inName) {
        return masterService.findByName(inName);
    }

    @Override
    public Iterable<Book> sortTitle() throws ValidatorException {
        return masterService.sortTitle();
    }

    @Override
    public Purchase addPurchase(Purchase pur) throws ValidatorException {
        return masterService.addPurchase(pur);
    }

    @Override
    public Set<Purchase> getAllPurchases() {
        return masterService.getAllPurchases();
    }

    @Override
    public Purchase removePurchase(Long id) throws IllegalArgumentException {
        return masterService.removePurchase(id);
    }

    @Override
    public Set<String> formatPurchases() {
        return masterService.formatPurchases();
    }

    @Override
    public List<Pair<Client, Long>> spentMost() {
        return masterService.spentMost();
    }

    @Override
    public List<Pair<Book, Integer>> mostBought() {
        return masterService.mostBought();
    }
}
