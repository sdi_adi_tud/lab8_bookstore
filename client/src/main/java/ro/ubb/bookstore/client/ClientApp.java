package ro.ubb.bookstore.client;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.bookstore.client.ui.InputOutput;
import ro.ubb.bookstore.common.MasterService;
import ro.ubb.bookstore.common.domain.Client;


public class ClientApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.bookstore.client.config"
                );
        MasterService service = context.getBean(MasterService.class);
        InputOutput start = new InputOutput(service);
        start.menu();
        System.out.println("later gater");
    }
}
