package ro.ubb.bookstore.common.domain;


import java.time.LocalDate;

public class Purchase extends BaseEntity<Long> {
    private String transactionDate;
    private Long client;
    private Long book;
    public static Purchase INVALID=new Purchase("-1,-1,-1,-1");
    public Purchase(String csv){
        String[] x=csv.split(",");
        super.setId(Long.parseLong(x[0]));
        this.client =Long.parseLong(x[1]);
        this.book = Long.parseLong(x[2]);
        this.transactionDate =x[3];
    }
    @Override
    public String csv(){
        return super.getId()+","+client+","+book+","+transactionDate;
    }
    public Purchase(Long client,Long book){
        transactionDate=LocalDate.now().toString();
        this.client=client;
        this.book=book;
    }
    public Purchase(Long client,Long book,String dt){
        transactionDate=dt;
        this.client=client;
        this.book=book;
    }
    public Purchase(LocalDate dt){
        transactionDate=dt.toString();
    }
    public Long getClient(){
        return client;
    }
    public Long getBook(){
        return book;
    }
    public String getDate(){
        return transactionDate;
    }
    @Override
    public String toString() {
        return "Purchase ID:"+super.getId()+"Client ID:" + client + "Book ID:" + book + " date:" + transactionDate ;
    }

}
