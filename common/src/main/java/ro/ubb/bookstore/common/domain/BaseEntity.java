package ro.ubb.bookstore.common.domain;

import java.io.Serializable;
import java.lang.reflect.Field;

/***
 * Base class for {@link Book} and {@link Client}
 * @param <ID> The type of the id.
 */
public class BaseEntity<ID> implements Serializable {
    /***
     * the current id of the entity
     */
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
    public String csv(){return  ""+id;}
    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                '}';
    }

}
