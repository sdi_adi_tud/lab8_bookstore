package ro.ubb.bookstore.common;

public class MasterServiceException extends RuntimeException {

        public  MasterServiceException(String message, Throwable cause) {
            super(message, cause);
        }

        public MasterServiceException(Throwable cause) {
            super(cause);
        }
    }

