package ro.ubb.bookstore.common;


import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
public interface MasterService {
    /* client operations*/
    public Client addClient(Client entity) throws ValidatorException;
    public Set<Client> getAllClients();
    public Client removeClient(Long id) throws IllegalArgumentException;
    public Client update(Client entity) throws ValidatorException;
    public Set<Client> findByAdress(String addr);

    /* book operations*/
    public Book addBook(Book entity) throws ValidatorException;
    public Set<Book> getAllBooks();
    public Book removeBook(Long id) throws IllegalArgumentException;
    public Book update(Book entity) throws ValidatorException;
    public Set<Book> findByYear(int year);
    public  Set<Book> findByName(String inName);
    public Iterable<Book> sortTitle() throws ValidatorException;
    /* purchase operations*/
    public Purchase addPurchase(Purchase pur) throws ValidatorException;
    public Set<Purchase> getAllPurchases();
    public Purchase removePurchase(Long id) throws IllegalArgumentException;

    /*master operations*/
    public Set<String> formatPurchases();
    public List<Pair<Client,Long>> spentMost();
    public List<Pair<Book,Integer>> mostBought();

}
