package ro.ubb.bookstore.common.domain.validators;

import ro.ubb.bookstore.common.domain.Purchase;

import java.time.LocalDate;

public class PurchaseValidator implements Validator<Purchase>{
    @Override
    public void validate(Purchase entity) throws ValidatorException {
        try{
            LocalDate.parse(entity.getDate());
        }catch(Exception e){throw new ValidatorException("Invalid date format:"+entity.getDate());}
    }
}
