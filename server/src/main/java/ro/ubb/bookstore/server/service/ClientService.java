package ro.ubb.bookstore.server.service;

import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;
import ro.ubb.bookstore.server.repo.SQLRepo.ClientRepository;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ClientService {
    private ClientRepository repo;

    /**
     * Constructor
     *
     */
    public ClientService(ClientRepository repo) {
        this.repo = repo;
    }

    /**
     * Add the {@code book} to the {@code repo}.
     *
     * @param client must be not null, {@link Client}
     *
     * @return The {@link Client} that was added.
     */
    public Optional<Client> addClient(Client client) throws ValidatorException {
        return repo.save(client);
    }
    public Optional<Client> findOne(Long id){return repo.findOne(id);}
    /**
     * Gets all clients from {@code repo}
     *
     * @return Set
     */
    public Set<Client> getAllClients() {
        return StreamSupport.stream(repo.findAll().spliterator(), false).collect(Collectors.toSet());
    }
    public Optional<Client> removeClient(Long id) throws IllegalArgumentException {
        return repo.delete(id);
    }
    public Optional<Client> update(Client client) throws ValidatorException {
        return repo.update(client);
    }
    public Set<Client> findByAdress(String addr){
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getAddress().toLowerCase().contains(addr.toLowerCase())).collect(Collectors.toSet());
    }
}
