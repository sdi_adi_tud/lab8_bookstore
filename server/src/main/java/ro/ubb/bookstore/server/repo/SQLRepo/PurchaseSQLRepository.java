package ro.ubb.bookstore.server.repo.SQLRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.Validator;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;

public class PurchaseSQLRepository implements PurchaseRepository {

    @Autowired
    private JdbcOperations jdbcOperations;
    private final Validator<Purchase> validator;

    public PurchaseSQLRepository(Validator<Purchase> validator) {
        this.validator = validator;
    }

    public Optional<Purchase> findOne(Long id) {
        String sql = "select * from purchase where purchaseid = ?";

        List<Purchase> query = jdbcOperations.query(sql, new Object[]{id}, (rs, rowNum) -> {
            Long clientid = rs.getLong("clientid");
            Long bookid = rs.getLong("bookid");
            String tdate = rs.getString("transactiondate");

            Purchase b = new Purchase(clientid, bookid, tdate);
            b.setId(id);

            return b;
        });

        if (query.isEmpty())
            return Optional.empty();

        return Optional.of(query.get(0));
    }

    public Iterable<Purchase> findAll() {
        String sql = "select * from purchase";
        return jdbcOperations.query(sql, (rs, rowNum) -> {
            long id = rs.getLong("clientid");
            Long clientid = rs.getLong("clientid");
            Long bookid = rs.getLong("bookid");
            String tdate = rs.getString("transactiondate");

            Purchase b = new Purchase(clientid, bookid, tdate);
            b.setId(id);

            return b;
        });
    }

    public Optional<Purchase> save(Purchase entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "insert into purchase (purchaseid, clientid, bookid, transactiondate) values (?, ?, ?, ?)";
        int updateCount = jdbcOperations.update(sql, entity.getId(), entity.getClient(),
                entity.getBook(), entity.getDate());

        if (updateCount == 0)
        {
            return Optional.of(entity);
        }
        return Optional.empty();
    }

    public Optional<Purchase> delete(Long id) {
        String sql = "delete from purchase where purchaseid = ?";
        Optional<Purchase> f = this.findOne(id);

        // If entity with given id is not present then return empty optional
        if (f.isEmpty())
            return f;

        jdbcOperations.update(sql, id);
        return f;
    }

    public Optional<Purchase> update(Purchase entity) {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "update purchase set clientid = ?, bookid = ?, transactiondate = ? where purchaseid = ?";
        int updateCount = jdbcOperations.update(sql, entity.getClient(),
                entity.getBook(), entity.getDate(), entity.getId());

        if (updateCount == 0)
        {
            return Optional.of(entity);
        }

        return Optional.empty();
    }
}
