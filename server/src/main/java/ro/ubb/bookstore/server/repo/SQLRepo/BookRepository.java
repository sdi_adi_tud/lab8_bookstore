package ro.ubb.bookstore.server.repo.SQLRepo;

import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.Optional;

public interface BookRepository {
    Optional<Book> findOne(Long id);

    Iterable<Book> findAll();

    Optional<Book> save(Book entity) throws ValidatorException;

    Optional<Book> delete(Long id);

    Optional<Book> update(Book entity) throws ValidatorException;
}
