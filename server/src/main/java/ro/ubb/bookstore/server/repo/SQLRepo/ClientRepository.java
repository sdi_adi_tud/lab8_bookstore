package ro.ubb.bookstore.server.repo.SQLRepo;

import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.Optional;

public interface ClientRepository {
    Optional<Client> findOne(Long id);

    Iterable<Client> findAll();

    Optional<Client> save(Client entity) throws ValidatorException;

    Optional<Client> delete(Long id);

    Optional<Client> update(Client entity) throws ValidatorException;
}
