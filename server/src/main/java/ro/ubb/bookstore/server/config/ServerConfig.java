package ro.ubb.bookstore.server.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import ro.ubb.bookstore.common.MasterService;
import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.*;
import ro.ubb.bookstore.server.repo.SQLRepo.*;
import ro.ubb.bookstore.server.service.BookService;
import ro.ubb.bookstore.server.service.ClientService;
import ro.ubb.bookstore.server.service.PurchaseService;
import ro.ubb.bookstore.server.service.ServerService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by radu.
 */
@Configuration
public class ServerConfig {
    @Bean
    RmiServiceExporter rmiServiceExporter() {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();
        rmiServiceExporter.setServiceName("MasterService");
        rmiServiceExporter.setServiceInterface(MasterService.class);
        rmiServiceExporter.setService(masterService());
        return rmiServiceExporter;
    }
    @Bean
    BookRepository bookRepository() {
        return new BookSQLRepository(new BookValidator());
    }
    @Bean
    ClientRepository clientRepository() {
        return new ClientSQLRepository(new ClientValidator());}
    @Bean
    PurchaseRepository purchaseRepository() {
        return new PurchaseSQLRepository(new PurchaseValidator());}
    @Bean
    MasterService masterService(){
        BookService bookService =new BookService(this.bookRepository());
        ClientService clientService =new ClientService(this.clientRepository());
        PurchaseService purchaseService=new PurchaseService(this.purchaseRepository());
        return new ServerService(clientService,bookService,purchaseService);
    }
}
