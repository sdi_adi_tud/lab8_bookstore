package ro.ubb.bookstore.server.repo.SQLRepo;

import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.Optional;

public interface PurchaseRepository {
    Optional<Purchase> findOne(Long id);

    Iterable<Purchase> findAll();

    Optional<Purchase> save(Purchase entity) throws ValidatorException;

    Optional<Purchase> delete(Long id);

    Optional<Purchase> update(Purchase entity) throws ValidatorException;
}
