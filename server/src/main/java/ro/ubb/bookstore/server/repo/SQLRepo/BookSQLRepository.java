package ro.ubb.bookstore.server.repo.SQLRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.validators.Validator;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;

public class BookSQLRepository implements BookRepository {
    @Autowired
    private JdbcOperations jdbcOperations;
    private final Validator<Book> validator;

    public BookSQLRepository(Validator<Book> validator) {
        this.validator = validator;
    }

    public Optional<Book> findOne(Long id) {
        String sql = "select * from books where bookid = ?";

        List<Book> query = jdbcOperations.query(sql, new Object[]{id}, (rs, rowNum) -> {
            String title = rs.getString("title");
            String author = rs.getString("author");
            long price = rs.getLong("price");
            int year = rs.getInt("year");

            Book b = new Book(title, author, price, year);
            b.setId(id);

            return b;
        });

        if (query.isEmpty())
            return Optional.empty();

        return Optional.of(query.get(0));
    }

    public Iterable<Book> findAll() {
        String sql = "select * from books";
        return jdbcOperations.query(sql, (rs, rowNum) -> {
            long id = rs.getLong("bookid");
            String title = rs.getString("title");
            String author = rs.getString("author");
            long price = rs.getLong("price");
            int year = rs.getInt("year");

            Book b = new Book(title, author, price, year);
            b.setId(id);

            return b;
        });
    }

    public Optional<Book> save(Book entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "insert into books (bookid, title, author, price, year) values (?, ?, ?, ?, ?) ";
        int updateCount = jdbcOperations.update(sql, entity.getId(), entity.getTitle(),
                entity.getAuthor() ,entity.getPrice(), entity.getYear());

        if (updateCount == 0)
        {
            return Optional.of(entity);
        }
        return Optional.empty();
    }

    public Optional<Book> delete(Long id) {
        String sql = "delete from books where bookid = ?";
        Optional<Book> f = this.findOne(id);

        // If entity with given id is not present then return empty optional
        if (f.isEmpty())
            return f;

        jdbcOperations.update(sql, id);
        return f;
    }

    public Optional<Book> update(Book entity) {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "update books set title = ?, author = ?, price = ?, year = ? where bookid = ?";
        int updateCount = jdbcOperations.update(sql, entity.getTitle(),
                entity.getAuthor(), entity.getPrice(), entity.getYear(), entity.getId());

        if (updateCount == 0)
        {
            return Optional.empty();
        }

        return Optional.of(entity);
    }
}
