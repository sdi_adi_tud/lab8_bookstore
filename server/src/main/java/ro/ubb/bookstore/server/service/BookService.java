package ro.ubb.bookstore.server.service;


import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import ro.ubb.bookstore.server.repo.SQLRepo.BookRepository;
import ro.ubb.bookstore.server.repo.SQLRepo.BookSQLRepository;
import ro.ubb.bookstore.server.repo.sort.Sort;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import ro.ubb.bookstore.common.domain.Book;

/**
 * Controller class for managing Book entities.
 *
 * @author Adi, Tudor
 *
 */
public class BookService {
    private BookRepository repo;

    /**
     * Constructor
     *
     * @param repo

     *
     */
    public BookService(BookRepository repo){this.repo=repo;}

    /**
     * Add the {@code book} to the {@code repo}.
     *
     * @param book
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been added.
     */
    public Optional<Book> addBook(Book book) throws ValidatorException{
       return repo.save(book);
    }

    /**
     * Gets a certain book from {@code repo}
     * @param id The id of the {@link Book} to fetch
     *
     * @return Set
     */
    public Optional<Book> findOne(Long id){return repo.findOne(id);}
    public Set<Book> getAllBooks(){
        return StreamSupport.stream(repo.findAll().spliterator(),false).collect(Collectors.toSet());
    }

    /**
     * Remove a book from the the {@code book repo}.
     *
     * @param id
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been removed.
     *
     */

    public Optional<Book> removeBook(Long id) throws IllegalArgumentException {
        return repo.delete(id);
    }

    /**
     * Updates a book from the the {@code book repo}.
     *
     * @param book
     *            must be not null, {@link Book}
     *
     * @return The {@link Book} that has been updated, none if no book was updated
     */
    public Optional<Book> update(Book book) throws ValidatorException {
        return repo.update(book);
    }
    public Iterable<Book> sortTitle() throws ValidatorException{
            return repo.findAll();
    }
    public Set<Book> findByYear(int year){
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getYear()==year).collect(Collectors.toSet());
    }
    public  Set<Book> findByName(String inName){
        return StreamSupport.stream(repo.findAll().spliterator(),false).filter(x->x.getTitle().toLowerCase().contains(inName.toLowerCase())).collect(Collectors.toSet());
    }
}
