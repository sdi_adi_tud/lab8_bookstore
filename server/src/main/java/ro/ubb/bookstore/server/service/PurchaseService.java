package ro.ubb.bookstore.server.service;

import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;
import ro.ubb.bookstore.server.repo.SQLRepo.PurchaseRepository;


import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PurchaseService {
    private PurchaseRepository repo;
    public PurchaseService(PurchaseRepository repo){this.repo=repo;}
    public Optional<Purchase> addPurchase(Purchase pur) throws ValidatorException{
        System.out.println("yoooo");
        return repo.save(pur);
    }
    public Set<Purchase> getAllPurchases(){
        return StreamSupport.stream(repo.findAll().spliterator(),false).collect(Collectors.toSet());
    }
    public Optional<Purchase> removePurchase(Long id) throws IllegalArgumentException{
        return repo.delete(id);
    }

}
