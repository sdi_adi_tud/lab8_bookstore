package ro.ubb.bookstore.server.service;

import ro.ubb.bookstore.common.Pair;
import ro.ubb.bookstore.common.MasterService;
import ro.ubb.bookstore.common.domain.Book;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.Purchase;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

public class ServerService implements MasterService {
    BookService book;
    ClientService client;
    PurchaseService purchase;
    public ServerService(ClientService clients, BookService books, PurchaseService purchases){book=books;client=clients;purchase=purchases;}
    /* client operations*/
    public Client addClient(Client entity) throws ValidatorException{Optional<Client> x=client.addClient(entity);
        return x.orElseGet(() -> Client.INVALID);
    }
    public Set<Client> getAllClients(){return client.getAllClients();}
    public Client removeClient(Long id) throws IllegalArgumentException {
        Optional<Client> ob=client.findOne(id);
        ob.ifPresent(value -> purchase.getAllPurchases().stream().filter(pur -> pur.getClient().equals(value.getId())).forEach(pur -> purchase.removePurchase(pur.getId())));
        ob = client.removeClient(id);
        return ob.orElseGet(() -> Client.INVALID);
    }
    public Client update(Client entity) throws ValidatorException {Optional<Client> x=client.update(entity);
        return x.orElseGet(() -> Client.INVALID);
    }
    public Set<Client> findByAdress(String addr){return client.findByAdress(addr);}

    /* book operations*/
    public Book addBook(Book entity) throws ValidatorException{Optional<Book> x=book.addBook(entity);
        return x.orElseGet(() -> Book.INVALID);
    }
    public Set<Book> getAllBooks(){return book.getAllBooks();}
    public Book removeBook(Long id) throws IllegalArgumentException {
        Optional<Book> ob=book.findOne(id);
        ob.ifPresent(value -> purchase.getAllPurchases().stream().filter(pur -> pur.getBook().equals(value.getId())).forEach(pur -> purchase.removePurchase(pur.getId())));
        ob = book.removeBook(id);
        return ob.orElseGet(() -> Book.INVALID);
    }
    public Book update(Book entity) throws ValidatorException {
        Optional<Book> x=book.update(entity);
        return x.orElseGet(() -> Book.INVALID);
    }
    public Set<Book> findByYear(int year){return book.findByYear(year);}
    public Set<Book> findByName(String inName){return book.findByName(inName);}
    public Iterable<Book> sortTitle() throws ValidatorException{return book.sortTitle();}
    /* purchase operations*/
    public Purchase addPurchase(Purchase pur) throws ValidatorException{
        Optional<Purchase> x=purchase.addPurchase(pur);
        return x.orElseGet(() -> Purchase.INVALID);
    }
    public Set<Purchase> getAllPurchases(){ return purchase.getAllPurchases();}
    public Purchase removePurchase(Long id) throws IllegalArgumentException{
        Optional<Purchase> x=purchase.removePurchase(id);
        return x.orElseGet(() -> Purchase.INVALID);
    }

    /*master operations*/
    public Set<String> formatPurchases(){ return purchase.getAllPurchases().stream().map(pur->
            client.findOne(pur.getClient()).get().getName()+" bought "+book.findOne(pur.getBook()).get().getTitle()+" on "+pur.getDate()).collect(Collectors.toSet());}
    public List<Pair<Client, Long>> spentMost(){
        return client.getAllClients().stream().map(
               client1->{Long spent=purchase.getAllPurchases().stream().filter(pur->pur.getClient()==client1.getId()).
                       map(pur->book.findOne(pur.getBook()).get().getPrice()).reduce(0L,(a, b)->a+b);
                    return new Pair<>(client1,spent);
               }
        ).sorted((a,b)->b.getValue().compareTo(a.getValue())).collect(Collectors.toList());
    }
    public List<Pair<Book, Integer>> mostBought(){
        return book.getAllBooks().stream().map(
                book1->{
                    Integer bought=Math.toIntExact(purchase.getAllPurchases().stream().filter(pur->pur.getBook()==book1.getId()).count());
                    return new Pair<>(book1,bought);
                }
        ).sorted((a,b)->b.getValue().compareTo(a.getValue())).collect(Collectors.toList());
    }
}
