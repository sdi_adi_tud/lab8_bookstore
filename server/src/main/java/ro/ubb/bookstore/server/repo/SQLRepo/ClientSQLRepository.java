package ro.ubb.bookstore.server.repo.SQLRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import ro.ubb.bookstore.common.domain.Client;
import ro.ubb.bookstore.common.domain.validators.Validator;
import ro.ubb.bookstore.common.domain.validators.ValidatorException;

import java.util.List;
import java.util.Optional;

public class ClientSQLRepository implements ClientRepository {

    @Autowired
    private JdbcOperations jdbcOperations;
    private final Validator<Client> validator;

    public ClientSQLRepository(Validator<Client> validator) {
        this.validator = validator;
    }

    public Optional<Client> findOne(Long id) {
        String sql = "select * from clients where clientid = ?";

        List<Client> query = jdbcOperations.query(sql, new Object[]{id}, (rs, rowNum) -> {
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String address = rs.getString("address");

            Client b = new Client(firstname, lastname, address);
            b.setId(id);

            return b;
        });

        if (query.isEmpty())
            return Optional.empty();

        return Optional.of(query.get(0));
    }

    public Iterable<Client> findAll() {
        String sql = "select * from clients";
        return jdbcOperations.query(sql, (rs, rowNum) -> {
            long id = rs.getLong("clientid");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String address = rs.getString("address");

            Client b = new Client(firstname, lastname, address);
            b.setId(id);

            return b;
        });
    }

    public Optional<Client> save(Client entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "insert into clients (clientid, firstname, lastname, address) values (?, ?, ?, ?) ";
        int updateCount = jdbcOperations.update(sql, entity.getId(), entity.getFirst(),
                entity.getLast(), entity.getAddress());

        if (updateCount == 0)
        {
            return Optional.of(entity);
        }
        return Optional.empty();
    }

    public Optional<Client> delete(Long id) {
        String sql = "delete from clients where clientid = ?";
        Optional<Client> f = this.findOne(id);

        // If entity with given id is not present then return empty optional
        if (f.isEmpty())
            return f;

        jdbcOperations.update(sql, id);
        return f;
    }

    public Optional<Client> update(Client entity) {
        if (entity == null) {
            throw new IllegalArgumentException("[ERROR - Repository - save()] Id must not be null");
        }

        validator.validate(entity);
        String sql = "update clients set firstname = ?, lastname = ?, address = ? where clientid = ? ";
        int updateCount = jdbcOperations.update(sql, entity.getFirst(),
                entity.getLast(), entity.getAddress(), entity.getId());

        if (updateCount == 0)
        {
            return Optional.empty();
        }

        return Optional.of(entity);
    }
}
