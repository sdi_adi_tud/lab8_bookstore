package ro.ubb.bookstore.server.repo.sort;

import ro.ubb.bookstore.common.domain.BaseEntity;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class SortStep{
    String field;
    int reverse;
    public SortStep(int reverse,String field){
        this.field=field;
        this.reverse=reverse;
    }
}
public class Sort {
    public static class Direction{
        static int DESC=1;
        static int ASC=0;
    }
    List<SortStep> steps=new ArrayList<>();
    public Sort(int direction,String ...fields){
        Arrays.stream(fields).forEach(field->steps.add(new SortStep(direction,field)));
    }
    public Sort(String ...fields){
        Arrays.stream(fields).forEach(field->steps.add(new SortStep(0,field)));
    }
    public Sort and(Sort other){
        steps=Stream.concat(steps.stream(),other.steps.stream()).collect(Collectors.toList());
        return this;
    }
    @SuppressWarnings("unchecked")
    private Iterable<?> sortOne(Iterable<?> collection,SortStep step) throws IllegalAccessException,NoSuchFieldException,IllegalArgumentException{
        Optional<?> any=StreamSupport.stream(collection.spliterator(),false).findAny();
        Object any1=any.orElseThrow(()-> {
            throw new IllegalArgumentException("Empty collection"); });
        Field field=any1.getClass().getDeclaredField(step.field);
        field.setAccessible(true);
        if(!(field.get(any1) instanceof Comparable)){
            throw new IllegalArgumentException("sort error.Property type not comparable");
        }
        collection=StreamSupport.stream(collection.spliterator(),false).sorted((a,b)-> {
            try {
                return ((Comparable)field.get(a)).compareTo((Comparable)field.get(b));
            } catch (IllegalAccessException e) {
                //for some reason it says "untreated" when i declare that function throws this type of exception..
                throw new IllegalArgumentException("Can't access field "+field.getName());
            }
        }).collect(Collectors.toList());
        if(step.reverse==1)
            Collections.reverse((List<?>) collection);
        field.setAccessible(false);
        return collection;
    }
    public Iterable<?> sort(Iterable<?> collection) throws IllegalAccessException,NoSuchFieldException,IllegalArgumentException {
        for(SortStep step:steps)
                collection=sortOne(collection,step);
        return collection;
    }
}
