package ro.ubb.bookstore.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.bookstore.server.repo.SQLRepo.BookRepository;

public class ServerApp {
    public static void main(String[] args) {
        System.out.println("server starting");
        AnnotationConfigApplicationContext context=
                new AnnotationConfigApplicationContext(
                        "ro.ubb.bookstore.server.config"
                );
    }


}


